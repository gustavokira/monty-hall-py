import random

class Porta:

    def __init__(self):
        self.aberta = False
        self.premio = False

class Jogador:
    def __init__(self):
        self.trocou = False
        self.porta = None

    def decidirSeVaiTrocar(self):
        idx = random.randint(0,1)
        if(idx == 1):
            self.trocou = True
        else:
            self.trocou = False

        return self.trocou

class Jogo:

    def __init__(self, quantidade):
        self.portas = []
        self.jogador = Jogador()

        for q in range(quantidade):
            self.portas.append(Porta())

    def colocarPremio(self):
        idx = random.randint(0,len(self.portas)-1)
        self.portas[idx].premio = True

    def escolherUmaPortaAleatoriamente(self):
        idx = random.randint(0,len(self.portas)-1)
        self.jogador.porta = self.portas[idx]

    def abrirUmaPorta(self):
        candidatas = []
        for porta in self.portas:
            if( porta != self.jogador.porta and porta.premio == False):
                candidatas.append(porta)
        idx = random.randint(0,len(candidatas)-1)
        candidatas[idx].aberta = True

    def trocarDePorta(self):
        trocar = self.jogador.decidirSeVaiTrocar()
        candidatas = []
        for porta in self.portas:
            if( porta != self.jogador.porta and porta.aberta == False):
                candidatas.append(porta)
        idx = random.randint(0,len(candidatas)-1)
        self.jogador.porta = candidatas[idx]

    def ganhou(self):
        return self.jogador.porta.premio

    def trocou(self):
        return self.jogador.trocou

class Simulacao:
    def __init__(self, quantidade):
        self.jogos = []
        self.quantidade = quantidade
        self.ganhou = 0
        self.perdeu = 0

    def rodarSemTrocarDePorta(self):
        self.rodar(False)

    def rodarTrocandoDePorta(self):
        self.rodar(True)

    def rodar(self, trocar):
        for q in range(self.quantidade):
            j = Jogo(3)
            j.colocarPremio()
            j.escolherUmaPortaAleatoriamente()
            j.abrirUmaPorta()
            if(trocar):
                j.trocarDePorta()


            ganhou = j.ganhou()
            if(ganhou):
                self.ganhou += 1
            else:
                self.perdeu += 1


            self.jogos.append(j)

    def resultado(self):
        saida = 'simulacoes:{}, ganhou:{}, perdeu:{}'.format(self.quantidade, self.ganhou, self.perdeu)
        print(saida)

quantidade = 10000
print('SEM TROCAR')
simSem = Simulacao(quantidade)
simSem.rodarSemTrocarDePorta()
simSem.resultado()

print('TROCANDO')
simTroc = Simulacao(quantidade)
simTroc.rodarTrocandoDePorta()
simTroc.resultado()
