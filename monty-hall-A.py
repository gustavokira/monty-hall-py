import random

quantidade = 10000
ganhouSemTrocar = 0
ganhouTrocando = 0
trocou = 0

for q in range(quantidade):
    escolha = -1
    novaEscolha = -1
    escolhaFinal = -1

    aberta = -1
    ganhou = 0

    premio = random.randint(0,2)

    escolha = random.randint(0,2)
    escolhaFinal = escolha

    portasParaAbrir = []

    for idx in range(0,3):
        if(idx != escolha and idx != premio):
            portasParaAbrir.append(idx)

    trocar = random.randint(0,1)

    idAbrir = random.randint(0,len(portasParaAbrir)-1)
    aberta = portasParaAbrir[idAbrir]

    if(trocar == 1):
        for idx, porta in enumerate(range(3)):
            if(idx != aberta and idx != escolha):
                novaEscolha = idx
                escolhaFinal = idx
                break

    if(escolhaFinal == premio):
        ganhou = 1

    saida = 'escolhaInicial:{}, aberta:{}, premio:{}, escolhaFinal:{}, trocar:{}, ganhou:{}'.format(escolha, aberta, premio, escolhaFinal, trocar, ganhou)
    print(saida)

    if(trocar == 1):
        trocou += 1

    if(ganhou == 1):
        if(trocar == 1):
            ganhouTrocando += 1
        else:
            ganhouSemTrocar += 1

ganhouSemTrocarPer = float(ganhouSemTrocar)/(quantidade-trocou)
ganhouTrocandoPer = float(ganhouTrocando)/trocou

saidaFinal = 'ganhou sem trocar:{:f}, ganhou trocando:{:f}'.format(ganhouSemTrocarPer, ganhouTrocandoPer)
print(saidaFinal)
